all: test lint-full

test:
	@echo ">> test"
	@go test ./... -coverprofile cover.out
	@go tool cover -html=cover.out -o cover.html

lint-full: lint card

lint-badge-full: lint card card-badge

card:
	@echo ">> card"
	@goreportcard-cli -v

lint:
	@echo ">> lint"
	@golangci-lint run

card-badge:
	@echo ">>card-badge"
	@curl -X GET 'https://goreportcard.com/checks?repo=gitlab.com/shadowy/go/postgres'
