package postgres

import (
	"github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"time"
)

// Listener - class for work with Postgres listener
type Listener struct {
	connection string
	shouldExit bool
	channel    string
	listener   *pq.Listener
	Data       chan string
}

func (listener Listener) init(connection, channel string) (result *Listener, err error) {
	data := Listener{connection: connection, shouldExit: false, channel: channel, Data: make(chan string)}
	result = &data
	err = result.start()
	return
}

func (listener Listener) start() error {
	logrus.WithFields(logrus.Fields{"channel": listener.channel}).Debug("postgres.Listener.start")
	listener.listener = pq.NewListener(listener.connection, 10*time.Second, time.Minute, listener.report)
	logrus.WithFields(logrus.Fields{"channel": listener.channel}).Debug("postgres.Listener.start listen")
	err := listener.listener.Listen(listener.channel)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"channel": listener.channel}).Error("postgres.Listener.start")
	} else {
		go listener.waitForNotification()
	}
	return err
}

func (listener Listener) waitForNotification() {
	for {
		if listener.shouldExit {
			logrus.WithFields(logrus.Fields{"channel": listener.channel}).Debug("postgres.Listener.waitForNotification exit")
			break
		}
		select {
		case data := <-listener.listener.Notify:
			logrus.WithFields(logrus.Fields{"channel": listener.channel, "data": data.Extra}).Debug("postgres.Listener.waitForNotification data")
			listener.Data <- data.Extra
			continue
		case <-time.After(90 * time.Second):
			logrus.WithFields(logrus.Fields{"channel": listener.channel}).Debug("postgres.Listener.waitForNotification ping")
			go func() {
				errProcess := listener.listener.Ping()
				if errProcess != nil {
					logrus.WithError(errProcess).WithFields(logrus.Fields{"channel": listener.channel}).Error("postgres.Listener.waitForNotification ping")
				}
			}()
		}
	}
}

func (listener Listener) report(event pq.ListenerEventType, err error) {
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"channel": listener.channel}).Error("postgres.Listener.report")
	}
}

// Close - close listener
func (listener *Listener) Close() {
	logrus.WithFields(logrus.Fields{"channel": listener.channel}).Debug("postgres.Listener.Close")
	listener.shouldExit = true
}
