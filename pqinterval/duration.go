package pqinterval

import (
	"database/sql/driver"
	"errors"
	"math"
	"time"
)

// NullDuration - Duration is a time.Duration alias that supports the driver.Valuer and
// sql.Scanner interfaces.
type NullDuration struct {
	Duration time.Duration
	Valid    bool
}

// ErrTooBig - returned by Interval.Duration and Duration.Scan if the
// interval would overflow a time.Duration.
var ErrTooBig = errors.New("interval overflows time.Duration")

// Duration - converts an Interval into a time.Duration with the same
// semantics as `EXTRACT(EPOCH from <interval>)` in PostgreSQL.
func (ival Interval) Duration() (time.Duration, error) {
	dur := int64(ival.Years())

	if dur > math.MaxInt64/nsPerYr || dur < math.MinInt64/nsPerYr {
		return 0, ErrTooBig
	}
	dur *= hrsPerYr
	dur += int64(ival.hrs)

	if dur > math.MaxInt64/int64(time.Hour) || dur < math.MinInt64/int64(time.Hour) {
		return 0, ErrTooBig
	}
	dur *= int64(time.Hour)

	us := ival.Microseconds() * int64(time.Microsecond)
	if dur > 0 {
		if math.MaxInt64-dur < us {
			return 0, ErrTooBig
		}
	} else {
		if math.MinInt64-dur > us {
			return 0, ErrTooBig
		}
	}
	dur += us

	return time.Duration(dur), nil
}

// Scan implements sql.Scanner.
func (d *NullDuration) Scan(src interface{}) error {
	if src == nil {
		d.Valid = false
		return nil
	}
	ival := Interval{}
	err := (&ival).Scan(src)
	if err != nil {
		return err
	}

	result, err := ival.Duration()
	if err != nil {
		return err
	}

	d.Duration = result
	d.Valid = true
	return nil
}

// Value implements driver.Valuer.
func (d NullDuration) Value() (driver.Value, error) {
	var years, months, days, hours, minutes, seconds, milliseconds, microseconds, nanoseconds int
	nanoseconds = int(d.Duration / time.Nanosecond)
	years, nanoseconds = divmod64(int64(nanoseconds), int64(time.Hour*hrsPerYr))
	days, nanoseconds = divmod64(int64(nanoseconds), int64(time.Hour*24))
	hours, nanoseconds = divmod64(int64(nanoseconds), int64(time.Hour))
	minutes, nanoseconds = divmod64(int64(nanoseconds), int64(time.Minute))
	seconds, nanoseconds = divmod64(int64(nanoseconds), int64(time.Second))
	milliseconds, nanoseconds = divmod64(int64(nanoseconds), int64(time.Millisecond))
	microseconds, _ = divmod(nanoseconds, int(time.Microsecond))
	return formatInput(years, months, days, hours, minutes, seconds, milliseconds, microseconds), nil
}
