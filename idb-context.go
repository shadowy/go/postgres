package postgres

import (
	"database/sql"
	"reflect"
)

// DBContextFunction - Callback function for receiving db connection
type DBContextFunction func(*sql.DB) error

// DBContextWithTransactionFunction - Callback function for receiving db transaction
type DBContextWithTransactionFunction func(*sql.Tx) error

// DBFieldMapping - Callback function for apply field mapping
type DBFieldMapping func(*sql.Rows) (interface{}, error)

// IDbContext - DB context common interface
type IDbContext interface {
	// Setup - setup Db context
	Setup(host string, port int, user string, password string, db string, sslMode string)
	// GetConnection - Get connection for DB
	GetConnection() (*sql.DB, error)
	// Using - helper for execute query with connection
	Using(query DBContextFunction) error
	// UsingWithTransaction - helper for execute query with transaction
	UsingWithTransaction(query DBContextWithTransactionFunction) error
	// GetRows - execute query what return list rows
	GetRows(query string, mapping DBFieldMapping, params ...interface{}) (data []interface{}, err error)
	// GetRowsModel - execute query what return list rows with model
	GetRowsModel(query string, model reflect.Type, params ...interface{}) (data []interface{}, err error)
	// GetSingleRow - execute query what return only one row
	GetSingleRow(query string, mapping DBFieldMapping, params ...interface{}) (data interface{}, err error)
	// GetSingleRowModel: execute query what return only one row with model
	GetSingleRowModel(query string, model reflect.Type, params ...interface{}) (data interface{}, err error)
	// ExecuteQuery - execute query
	ExecuteQuery(query string, params ...interface{}) error
}
