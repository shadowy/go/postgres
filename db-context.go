package postgres

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq" // add postgres
	"github.com/sirupsen/logrus"
	"reflect"
	"time"
)

// DbContext - db context
type DbContext struct {
	IDbContext

	host             string // host
	port             int    // port
	user             string // user
	password         string // password
	db               string // db
	sslMode          string // ssl mode
	connectionString string // connection string
	pool             *Pool  // connection pool
}

// Setup - setup Db context
func (context *DbContext) Setup(host string, port int, user, password, db, sslMode string, poolSize *int,
	timeLife *time.Duration) {
	context.host = host
	context.port = port
	context.user = user
	context.password = password
	context.db = db
	context.sslMode = sslMode
	context.pool = new(Pool)
	context.pool.Init(poolSize, timeLife, func() (db *sql.DB, err error) {
		db, err = context.createConnection()
		return
	})
	context.connectionString = fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=%s", context.user,
		context.password, context.host, context.port, context.db, context.sslMode)
}

// GetConnection - get connection for DB
func (context *DbContext) GetConnection() (*sql.DB, error) {
	logrus.Debug("DbContext.GetConnection")
	db, err := context.pool.Get()
	return db, err
}

// GetListener - get listener
func (context *DbContext) GetListener(channel string) (*Listener, error) {
	logrus.WithField("channel", channel).Debug("DbContext.GetListener")
	return Listener{}.init(context.connectionString, channel)
}

// Using - helper for execute query with connection
func (context *DbContext) Using(query DBContextFunction) error {
	var connection, err = context.GetConnection()
	if err != nil {
		return err
	}
	defer func() {
		err = context.pool.Release(connection)
		if err != nil {
			logrus.WithError(err).Error("DbContext.Using defer")
		}
	}()
	return query(connection)
}

// UsingWithTransaction - helper for execute query with transaction
func (context *DbContext) UsingWithTransaction(query DBContextWithTransactionFunction) error {
	var connection, err = context.GetConnection()
	if err != nil {
		return err
	}
	defer func() {
		err = context.pool.Release(connection)
		if err != nil {
			logrus.WithError(err).Error("DbContext.UsingWithTransaction defer")
		}
	}()
	// Start transaction
	transaction, err := connection.Begin()
	if err != nil {
		logrus.WithError(err).Error("DbContext.UsingWithTransaction can't open transaction")
		return err
	}
	skipRollback := false

	// Rollback transaction on exit
	defer func() {
		if skipRollback {
			return
		}
		err = transaction.Rollback()
		if err != nil {
			logrus.WithError(err).Error("DBContext.UsingWithTransaction")
		}
	}()

	// Execute query
	err = query(transaction)
	if err == nil {
		err = transaction.Commit()
		skipRollback = true
		return nil
	}
	return err
}

// Mapper - define rule for mapping sql row to model
func (context *DbContext) Mapper(model reflect.Type) DBFieldMapping {
	return func(rows *sql.Rows) (row interface{}, err error) {
		row, err = Mapper(rows, model)
		return
	}
}

// Mapping - mapping sql row to model collection
func (context *DbContext) Mapping(rows *sql.Rows, mapping DBFieldMapping) (data []interface{}, err error) {
	defer func() {
		err = rows.Close()
		if err != nil {
			logrus.WithError(err).Error("DbContext.Mapping close")
		}
	}()

	for rows.Next() {
		row, pErr := mapping(rows)
		if pErr == sql.ErrNoRows {
			continue
		}
		if pErr != nil {
			logrus.WithError(pErr).Error("DbContext.Mapping mapping")
			continue
		}
		data = append(data, row)
	}
	return
}

func (context *DbContext) getRows(connection QueryExecutor, query string, mapping DBFieldMapping, params ...interface{}) (
	data []interface{}, err error) {
	logrus.WithFields(logrus.Fields{"sql": query, "params": params}).Debug("DbContext.getRows")
	rows, err := connection.Query(query, params...)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"query": query, "params": params}).Error("DbContext.getRows")
		return
	}
	data, err = context.Mapping(rows, mapping)
	return
}

/**
First - return firs item from list
*/
func (context *DbContext) First(rows []interface{}) interface{} {
	if len(rows) > 0 {
		return rows[0]
	}
	return nil
}

// GetRows - execute query what return list rows
func (context *DbContext) GetRows(query string, mapping DBFieldMapping, params ...interface{}) (data []interface{}, err error) {
	logrus.WithFields(logrus.Fields{"sql": query, "params": params}).Debug("DbContext.GetRows")
	connection, params := context.getConnection(params)
	if connection != nil {
		return context.getRows(connection, query, mapping, params...)
	}

	err = context.Using(func(connection *sql.DB) error {
		list, pErr := context.getRows(connection, query, mapping, params...)
		data = list
		return pErr
	})
	return
}

// GetRowsModel - execute query what return list rows with model
func (context *DbContext) GetRowsModel(query string, model reflect.Type, params ...interface{}) (data []interface{}, err error) {
	logrus.WithFields(logrus.Fields{"sql": query, "params": params}).Debug("DbContext.GetRowsModel")
	data, err = context.GetRows(query, context.Mapper(model), params...)
	return
}

// GetSingleRow - execute query what return only one row
func (context *DbContext) GetSingleRow(query string, mapping DBFieldMapping, params ...interface{}) (data interface{}, err error) {
	logrus.WithFields(logrus.Fields{"sql": query, "params": params}).Debug("DbContext.GetSingleRow")
	var list []interface{}
	list, err = context.GetRows(query, mapping, params...)
	if err != nil {
		return
	}
	data = context.First(list)
	return
}

// GetSingleRowModel - execute query what return only one row with model
func (context *DbContext) GetSingleRowModel(query string, model reflect.Type, params ...interface{}) (data interface{}, err error) {
	logrus.WithFields(logrus.Fields{"sql": query, "params": params}).Debug("DbContext.GetSingleRowModel")
	var list []interface{}
	list, err = context.GetRowsModel(query, model, params...)
	if err != nil {
		return
	}
	data = context.First(list)
	return
}

// ExecuteQuery - execute query
func (context *DbContext) ExecuteQuery(query string, params ...interface{}) error {
	logrus.WithFields(logrus.Fields{"sql": query, "params": params}).Debug("ExecuteQuery")
	connection, params := context.getConnection(params)
	if connection != nil {
		_, err := connection.Exec(query, params...)
		if err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{"query": query, "params": params}).Error("ExecuteQuery")
		}
		return err
	}
	return context.UsingWithTransaction(func(transaction *sql.Tx) error {
		var _, err = transaction.Exec(query, params...)
		if err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{"query": query, "params": params}).Error("ExecuteQuery")
		}
		return err
	})
}

// createConnection - create connection for DB
func (context *DbContext) createConnection() (*sql.DB, error) {
	logrus.WithField("connectionString", context.connectionString).Debug("DbContext.createConnection")
	// Open connection to db
	var db, err = sql.Open("postgres", context.connectionString)
	if err != nil {
		logrus.WithError(err).Error("DbContext.createConnection Error connect to db")
		return nil, err
	}
	// Check connection to DB
	err = db.Ping()
	if err != nil {
		logrus.WithError(err).Error("DbContext.createConnection Error ping db")
		return nil, err
	}
	return db, nil
}

func (context *DbContext) getConnection(params []interface{}) (QueryExecutor, []interface{}) {
	if len(params) == 0 {
		return nil, params
	}
	obj := params[0]
	switch v := obj.(type) {
	case QueryExecutor:
		return v, params[1:]
	default:
		return nil, params
	}
}
