package postgres

type Database struct {
	DbContext
	Host     string  `yaml:"host"`
	Port     int     `yaml:"port"`
	User     string  `yaml:"user"`
	Password string  `yaml:"password"`
	Db       string  `yaml:"db"`
	SslMode  *string `yaml:"sslMode"`
	PoolSize *int    `yaml:"poolSize"`
}

func (cfg *Database) Init() error {
	if cfg.SslMode == nil {
		sslMode := "disable"
		cfg.SslMode = &sslMode
	}
	cfg.Setup(cfg.Host, cfg.Port, cfg.User, cfg.Password, cfg.Db, *cfg.SslMode, cfg.PoolSize, nil)
	return nil
}

func (cfg *Database) HealthName() string {
	return "DataBase: PostgreSQL"
}

func (cfg *Database) HealthCode() string {
	return "db.postgreSQL"
}

func (cfg *Database) HealthCheck() (bool, error) {
	err := cfg.ExecuteQuery("")
	if err != nil {
		return false, err
	}
	return true, nil
}
