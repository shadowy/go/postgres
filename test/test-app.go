package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/postgres"
	"reflect"
	"time"
)

type testRow struct {
	ID     int64     `db:"id"`
	String string    `db:"string"`
	Time   time.Time `db:"time"`
	Float  float32   `db:"float"`
}

func main() {
	var context postgres.DbContext
	context.Setup("127.0.0.1", 5432, "postgres", "postgres", "postgres", "disable", nil, nil)

	list, err := context.GetRowsModel("select * from test", reflect.TypeOf(testRow{}))
	if err != nil {
		fmt.Printf("%s\n", err)
	} else {
		for _, item := range list {
			fmt.Printf("%#v\n", item)
		}
	}
	listener, err := context.GetListener("test")
	logrus.WithError(err).Error("GetListener")
	for {
		data := <-listener.Data
		logrus.WithField("data", data).Error("get")
	}
}
