package postgres

import (
	"time"
)

// PoolConnection - connection in pool
type PoolConnection struct {
	CreateTime time.Time // time of creation
	isActive   bool      // connection is active
}
