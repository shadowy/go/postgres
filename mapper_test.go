package postgres

import (
	"encoding/json"
	"fmt"
	"reflect"
	"testing"
	"time"
)

type TmpJSON struct {
	ID int64 `json:"id"`
}

type Tmp struct {
	ID       int            `db:"id"`
	StringV  string         `db:"string"`
	Text     *string        `db:"text"`
	SmallInt *int16         `db:"small_int"`
	IntV     *int           `db:"int"`
	Bigint   *int64         `db:"bigint"`
	Real     float32        `db:"real"`
	Double   *float64       `db:"double"`
	Time     *time.Time     `db:"time"`
	TimeZ    *time.Time     `db:"timez"`
	TimeT    *time.Time     `db:"timet"`
	TimeTZ   *time.Time     `db:"timetz"`
	Interval *time.Duration `db:"interval"`
	Flag     *bool          `db:"flag"`
	JSON     TmpJSON        `db:"json"`
}

var (
	datajson []byte
)

type User struct {
	Name string
}

func MustJSONEncode(i interface{}) []byte {
	result, err := json.Marshal(i)
	if err != nil {
		panic(err)
	}
	return result
}

func Store(a interface{}) {
	datajson = MustJSONEncode(a)
	fmt.Println(string(datajson))
}

func Get(a []byte, b interface{}) interface{} {
	objType := reflect.TypeOf(b).Elem()
	obj := reflect.New(objType).Interface()
	// fmt.Println(obj)
	// MustJSONDecode(a, &obj)
	_ = json.Unmarshal(a, &obj)
	return obj
}

func TestApplicationPathWithoutFlag(t *testing.T) {
	dummy := &User{}
	david := User{Name: "DavidMahon"}

	Store(david)
	Get(datajson, dummy)

	var context = DbContext{}
	context.Setup("localhost", 5432, "sms", "sms", "sms", "disable", nil, nil)
	rows, err := context.GetRowsModel("select * from public.test", reflect.TypeOf(Tmp{}))
	fmt.Println(rows)
	fmt.Println(err)
}
