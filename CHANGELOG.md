<a name="unreleased"></a>
## [Unreleased]


<a name="v1.0.12"></a>
## [v1.0.12] - 2019-11-09
### Bug Fixes
- issue with transaction


<a name="v1.0.11"></a>
## [v1.0.11] - 2019-11-09
### Bug Fixes
- fix issue with get connection from parameters


<a name="v1.0.10"></a>
## [v1.0.10] - 2019-11-09
### Features
- add possibility to use one connection for all command


<a name="v1.0.9"></a>
## [v1.0.9] - 2019-11-08
### Code Refactoring
- created separate methods for Mapping and Mapping


<a name="v1.0.8"></a>
## [v1.0.8] - 2019-11-07
### Bug Fixes
- fixed the issue when showing error messages in a log


<a name="v1.0.7"></a>
## [v1.0.7] - 2019-10-25
### Bug Fixes
- issue with mapping data


<a name="v1.0.6"></a>
## [v1.0.6] - 2019-10-22
### Features
- add support bytea


<a name="v1.0.5"></a>
## [v1.0.5] - 2019-10-22
### Bug Fixes
- remove unused references


<a name="v1.0.4"></a>
## [v1.0.4] - 2019-07-18

<a name="v1.0.3"></a>
## [v1.0.3] - 2019-07-18

<a name="v1.0.2"></a>
## [v1.0.2] - 2019-07-17
### Features
- add database settings


<a name="v1.0.1"></a>
## v1.0.1 - 2019-07-16

[Unreleased]: https://gitlab.com/shadowy/go/postgres/compare/v1.0.12...HEAD
[v1.0.12]: https://gitlab.com/shadowy/go/postgres/compare/v1.0.11...v1.0.12
[v1.0.11]: https://gitlab.com/shadowy/go/postgres/compare/v1.0.10...v1.0.11
[v1.0.10]: https://gitlab.com/shadowy/go/postgres/compare/v1.0.9...v1.0.10
[v1.0.9]: https://gitlab.com/shadowy/go/postgres/compare/v1.0.8...v1.0.9
[v1.0.8]: https://gitlab.com/shadowy/go/postgres/compare/v1.0.7...v1.0.8
[v1.0.7]: https://gitlab.com/shadowy/go/postgres/compare/v1.0.6...v1.0.7
[v1.0.6]: https://gitlab.com/shadowy/go/postgres/compare/v1.0.5...v1.0.6
[v1.0.5]: https://gitlab.com/shadowy/go/postgres/compare/v1.0.4...v1.0.5
[v1.0.4]: https://gitlab.com/shadowy/go/postgres/compare/v1.0.3...v1.0.4
[v1.0.3]: https://gitlab.com/shadowy/go/postgres/compare/v1.0.2...v1.0.3
[v1.0.2]: https://gitlab.com/shadowy/go/postgres/compare/v1.0.1...v1.0.2
