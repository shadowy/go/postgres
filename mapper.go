package postgres

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/postgres/pqinterval"
	"reflect"
	"strings"
)

/**
Db mapping structure
*/
type dbMapping map[string]string

/**
Cache for Db mapping
*/
var cacheDbMapping = map[reflect.Type]dbMapping{}

type getType func() interface{}

func getInt64Array() interface{} {
	return &pq.Int64Array{}
}

func getInt64() interface{} {
	return &sql.NullInt64{}
}

func getStringArray() interface{} {
	return &pq.StringArray{}
}

func getString() interface{} {
	return &sql.NullString{}
}

func getFloat64Array() interface{} {
	return &pq.Float64Array{}
}

func getFloat64() interface{} {
	return &sql.NullFloat64{}
}

func getBoolArray() interface{} {
	return &pq.BoolArray{}
}

func getBool() interface{} {
	return &sql.NullBool{}
}

func getTime() interface{} {
	return &pq.NullTime{}
}

func getDuration() interface{} {
	return &pqinterval.NullDuration{}
}

var mapDbArrayType = map[string]getType{
	"int2":    getInt64Array,
	"int4":    getInt64Array,
	"int8":    getInt64Array,
	"varchar": getStringArray,
	"text":    getStringArray,
	"json":    getStringArray,
	"jsonb":   getStringArray,
	"name":    getStringArray,
	"float4":  getFloat64Array,
	"float8":  getFloat64Array,
	"numeric": getFloat64Array,
	"bool":    getBoolArray,
}

var mapDbType = map[string]getType{
	"int2":        getInt64,
	"int4":        getInt64,
	"int8":        getInt64,
	"varchar":     getString,
	"text":        getString,
	"json":        getString,
	"jsonb":       getString,
	"name":        getString,
	"float4":      getFloat64,
	"float8":      getFloat64,
	"numeric":     getFloat64,
	"timestamp":   getTime,
	"timestamptz": getTime,
	"time":        getTime,
	"timetz":      getTime,
	"interval":    getDuration,
	"bool":        getBool,
}

/**
Get variable for db type
*/
func getVarByDbType(dbType string) interface{} {
	dbType = strings.ToLower(dbType)
	if dbType[0] == '_' {
		if val, ok := mapDbArrayType[dbType]; ok {
			return val()
		}
		return &pq.GenericArray{}
	}
	if val, ok := mapDbType[dbType]; ok {
		return val()
	}
	if dbType == "bytea" {
		var bytes []byte
		return &bytes
	}
	fmt.Println(dbType)
	return nil
}

/**
Get or build db mapping for current type
*/
func getDbMapping(value reflect.Type) dbMapping {
	if val, ok := cacheDbMapping[value]; ok {
		return val
	}
	var mapping = dbMapping{}
	cacheDbMapping[value] = mapping
	for i := 0; i < value.NumField(); i++ {
		f := value.Field(i)
		tag := f.Tag.Get("db")
		if tag == "" {
			continue
		}
		mapping[f.Name] = tag
	}
	return mapping
}

func setString(v *sql.NullString, field *reflect.Value, dbType string) {
	if v.Valid {
		dbType = strings.ToLower(dbType)
		if (dbType == "json" || dbType == "jsonb") && field.Type().Name() != "string" {
			if field.Kind() == reflect.Ptr {
				d := reflect.New(field.Type().Elem()).Interface()
				err := json.Unmarshal([]byte(v.String), &d)
				if err != nil {
					logrus.WithError(err).Error("mapper.setValue ptr json, jsonb")
				}
				field.Set(reflect.ValueOf(d))
			} else {
				tp := reflect.PtrTo(field.Type())
				d := reflect.New(tp).Interface()
				err := json.Unmarshal([]byte(v.String), &d)
				if err != nil {
					logrus.WithError(err).Error("mapper.setValue json, jsonb")
				}
				field.Set(reflect.ValueOf(d))
			}
		} else {
			if field.Kind() == reflect.Ptr {
				fv := reflect.New(field.Type().Elem())
				fv.Elem().SetString(v.String)
				field.Set(fv)
			} else {
				field.SetString(v.String)
			}
		}
	}
}

func setInt(v *sql.NullInt64, field *reflect.Value) {
	if v.Valid {
		if field.Kind() == reflect.Ptr {
			fv := reflect.New(field.Type().Elem())
			fv.Elem().SetInt(v.Int64)
			field.Set(fv)
		} else {
			field.SetInt(v.Int64)
		}
	}
}

func setFloat(v *sql.NullFloat64, field *reflect.Value) {
	if v.Valid {
		if field.Kind() == reflect.Ptr {
			fv := reflect.New(field.Type().Elem())
			fv.Elem().SetFloat(v.Float64)
			field.Set(fv)
		} else {
			field.SetFloat(v.Float64)
		}
	}
}

func setBool(v *sql.NullBool, field *reflect.Value) {
	if v.Valid {
		if field.Kind() == reflect.Ptr {
			fv := reflect.New(field.Type().Elem())
			fv.Elem().SetBool(v.Bool)
			field.Set(fv)
		} else {
			field.SetBool(v.Bool)
		}
	}
}

func setTime(v *pq.NullTime, field *reflect.Value) {
	if v.Valid {
		if field.Kind() == reflect.Ptr {
			fv := reflect.New(field.Type().Elem())
			fv.Elem().Set(reflect.ValueOf(v.Time))
			field.Set(fv)
		} else {
			field.Set(reflect.ValueOf(v.Time))
		}
	}
}

func setDuration(v *pqinterval.NullDuration, field *reflect.Value) {
	if v.Valid {
		if field.Kind() == reflect.Ptr {
			fv := reflect.New(field.Type().Elem())
			fv.Elem().Set(reflect.ValueOf(v.Duration))
			field.Set(fv)
		} else {
			field.Set(reflect.ValueOf(v.Duration))
		}
	}
}

func setBytes(data *[]byte, field *reflect.Value) {
	if field.Kind() == reflect.Ptr {
		fv := reflect.New(field.Type().Elem())
		fv.Elem().SetBytes(*data)
		field.Set(fv)
	} else {
		field.SetBytes(*data)
	}
}

/**
Set property value
*/
func setValue(data interface{}, field *reflect.Value, dbType string) {
	switch v := data.(type) {
	case *sql.NullString:
		setString(v, field, dbType)
	case *sql.NullInt64:
		setInt(v, field)
	case *sql.NullFloat64:
		setFloat(v, field)
	case *sql.NullBool:
		setBool(v, field)
	case *pq.NullTime:
		setTime(v, field)
	case *pqinterval.NullDuration:
		setDuration(v, field)
	case *[]uint8:
		setBytes(v, field)
	}
}

// Mapper - map db row to struct
func Mapper(row *sql.Rows, model reflect.Type) (obj interface{}, err error) {
	// Get columns types
	columnsType, err := row.ColumnTypes()
	if err != nil {
		logrus.WithError(err).Error("Mapper get column types")
		return
	}
	/**
	Build helper for mapping
	*/
	var columnsData []interface{}
	var mapColumns = map[string]int{}
	for pos, columnType := range columnsType {
		var v = getVarByDbType(columnType.DatabaseTypeName())
		columnsData = append(columnsData, v)
		mapColumns[columnType.Name()] = pos
	}
	/**
	Get Data
	*/
	err = row.Scan(columnsData...)
	if err != nil {
		logrus.WithError(err).Error("Mapper scan")
		return
	}
	/**
	Create new object
	*/
	tmp := reflect.New(model).Elem()
	mapping := getDbMapping(model)
	/**
	Set value for object
	*/
	for dst, src := range mapping {
		pos, ok := mapColumns[src]
		if !ok {
			continue
		}
		c := columnsData[pos]
		columnType := columnsType[pos]
		f := tmp.FieldByName(dst)
		setValue(c, &f, columnType.DatabaseTypeName())
	}

	return tmp.Interface(), nil
}
