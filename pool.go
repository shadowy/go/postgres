package postgres

import (
	"database/sql"
	"github.com/sirupsen/logrus"
	"sync"
	"time"
)

// CreateConnection - Function for create connection
type CreateConnection func() (*sql.DB, error)

// Pool - pool of db connection
type Pool struct {
	size              int                         // size of pool
	timeLife          *time.Duration              // life time of pool item
	create            CreateConnection            // create connection to DB
	list              map[*sql.DB]*PoolConnection // pool of connection
	activeConnections int                         // counter of active connection
	mutex             *sync.Mutex                 // mutex for pool (block get)
	limit             chan struct{}               // limit for number of connections
	lastTimeReduce    time.Time                   // time when execute reduce of pool
}

// Init - init pool
func (pool *Pool) Init(size *int, timeLife *time.Duration, create CreateConnection) {
	logrus.WithFields(logrus.Fields{"size": size, "timeLife": timeLife}).Debug("pool.Init")
	pool.create = create
	pool.mutex = &sync.Mutex{}
	pool.activeConnections = 0
	pool.list = make(map[*sql.DB]*PoolConnection)

	if timeLife == nil {
		defaultTime := time.Hour
		pool.timeLife = &defaultTime
	} else {
		pool.timeLife = timeLife
	}
	if size == nil {
		pool.size = 5
	} else {
		pool.size = *size
		if pool.size < 0 {
			pool.size = 0
		}
	}
	if pool.size > 0 {
		pool.limit = make(chan struct{}, pool.size)
	}
	logrus.WithFields(logrus.Fields{"size": pool.size, "timeLife": pool.timeLife}).Debug("pool.Init created")
}

// Get - get connection
func (pool *Pool) Get() (*sql.DB, error) {
	logrus.Debug("pool.Get")
	if pool.size == 0 {
		db, err := pool.create()
		return db, err
	}

	pool.mutex.Lock()
	defer pool.mutex.Unlock()
	pool.reduce()

	for db, info := range pool.list {
		if info.isActive {
			continue
		}
		info.isActive = true
		_, err := db.Exec("select 1")
		if err != nil {
			logrus.WithError(err).Debug("pool.Get connection died")
			delete(pool.list, db)
			continue
		}
		pool.activeConnections++
		pool.limit <- struct{}{}
		logrus.WithFields(pool.params()).Debug("pool.Get return from pool")
		return db, nil
	}

	pool.limit <- struct{}{}
	db, err := pool.create()
	if err != nil {
		<-pool.limit
		return nil, err
	}
	pool.activeConnections++
	item := PoolConnection{isActive: true, CreateTime: time.Now()}
	pool.list[db] = &item
	logrus.WithFields(pool.params()).Debug("pool.Get return new")
	return db, nil
}

// Release - close connection or put it back to pool
func (pool *Pool) Release(db *sql.DB) error {
	logrus.Debug("pool.Release")
	if pool.size == 0 {
		return db.Close()
	}

	pool.activeConnections--
	if item, ok := pool.list[db]; ok {
		item.isActive = false
	}
	<-pool.limit
	logrus.WithFields(pool.params()).Debug("Release return to pool")
	return nil
}

// reduce - remove all expired connection from pool
func (pool *Pool) reduce() {
	if pool.timeLife == nil {
		return
	}
	now := time.Now()
	delta := now.Sub(pool.lastTimeReduce)
	if delta < time.Minute {
		return
	}
	timeLife := *pool.timeLife
	logrus.Debug("pool.reduce")
	for db, info := range pool.list {
		if !info.isActive {
			delta := now.Sub(info.CreateTime)
			if delta > timeLife {
				err := db.Close()
				if err != nil {
					logrus.WithError(err).Error("pool.reduce")
				}
				delete(pool.list, db)
			}
		}
	}
}

func (pool *Pool) params() logrus.Fields {
	return logrus.Fields{
		"maxConnection":     pool.size,
		"activeConnections": pool.activeConnections,
		"count":             len(pool.list),
	}
}
